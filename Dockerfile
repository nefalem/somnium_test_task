# Dockerfile
FROM python:3.7
RUN mkdir /somnium_app
WORKDIR /somnium_app
COPY requirements.txt* /somnium_app/
RUN pip install -r requirements.txt
ADD somnium_app /somnium_app/
