from .views import TagsApiView
from django.urls import path

urlpatterns = [
    path('tags/', TagsApiView.as_view()),
    path('tags/<uuid:url_id>', TagsApiView.as_view()),
]