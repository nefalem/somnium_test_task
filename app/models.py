import uuid
from django.db import models


# Create your models here.
class UrlInfo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.URLField()
    url_wrong = models.BooleanField(default=False)
    url_checked = models.BooleanField(default=False)
    status_code = models.CharField(max_length=10)
    response_content = models.TextField(default=0, blank=True)
