import requests

from celery import shared_task
from app.models import UrlInfo

@shared_task
def get_url_info():
    urls = UrlInfo.objects.filter(url_checked=False)

    for i in urls:
        try:
            response = requests.get(url=i.url)
            if response.status_code == 200:
                i.status_code = response.status_code
                i.response_content = response.text
                i.url_checked = True
                i.save()

            else:
                i.url_wrong = True
                i.url_checked = True
                i.save()
        except:
            i.url_wrong = True
            i.url_checked = True
            i.save()
            print('Error Wrong URL or system error')

    print("Hello from Celery")
