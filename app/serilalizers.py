from rest_framework import serializers
from .models import UrlInfo

class URLSerializer(serializers.Serializer):
    url = serializers.URLField(max_length=200, min_length=None, allow_blank=False)

    def validate_url(self, value):
        if len(value) < 10:
            raise serializers.ValidationError(
                ("Wrong url")
            )
        return value

    def create(self, validated_data):
        url_info = UrlInfo.objects.create(**validated_data)
        return  url_info
