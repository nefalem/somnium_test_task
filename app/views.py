from lxml import html
from collections import Counter

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404

from .serilalizers import URLSerializer
from .models import UrlInfo


class TagsApiView(APIView):

    def post(self, request):
        """Обработка POST запросов"""
        serializer = URLSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            instance = serializer.save()
            return Response({'success': 'True', 'url_id': instance.id}, status=201)

    def get(self, request, url_id):
        """Обработка GET запросов"""

        url_info = get_object_or_404(UrlInfo, id=url_id)

        if url_info.url_checked is False:
            return Response({'url': 'while not checked, please wait'})

        if url_info.url_wrong is True:
            return Response({'url': 'you url is wrong or not found'})

        response_data = {
            'url_id': url_info.id,
            'url': url_info.url,
            'status_code': url_info.status_code,
            'html_tags': ''
        }

        html_tags = {}
        tree = html.fromstring(url_info.response_content)
        all_elms = tree.cssselect('*')
        all_tags = [x.tag for x in all_elms]
        c = Counter(all_tags)
        print(c)
        for e in c:
            html_tags.update({e: c[e]})
        response_data.update(html_tags=html_tags)
        print(response_data)
        return Response(response_data, status=200)